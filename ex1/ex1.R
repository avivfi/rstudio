df <- Orange

df[5,2]<-NA
df[7,2]<-NA
df[18,2]<-NA
df[28,2]<-NA

install.packages('mice')
library('mice')

tempDataMean <- mice(df,m=1,maxit = 1,meth ='mean',seed = 500)
tempDataMean$imp$age

completeDataMean <- complete(tempDataMean,1)
completeDataMean

tempDataPmm <- mice(df,m=5,maxit = 50,meth ='pmm',seed = 500)
completeDataPmm <- complete(tempDataPmm,1)

write.table(completeDataMean ,file = 'Mean.csv',sep = ',',col.names =  TRUE, row.names = FALSE) 
write.table(completeDataPmm ,file = 'Pmm.csv',sep = ',',col.names =  TRUE, row.names = FALSE) 
